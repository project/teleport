Teleport module
Created in 2008 by Dan Kurtz (incidentist)
Licensed under the GPL

This module is intended for administrators who have to jump around to specific pages while managing Drupal sites. It's a hotkey-activated launcher utility inspired by <a href="http://docs.blacktree.com/quicksilver/what_is_quicksilver">Quicksilver</a>. The "T" accesskey brings up the dialog (Ctrl-T on a Mac, Alt-T on Windows, apparently). Type in any part of the title or path of the page. Unlike many other dynamic search modules, Teleport searches menu items as well as node content.

It doesn't work without JavaScript, but it doesn't mess anything up either.