// JavaScript for Teleport module

Drupal.Teleport = {};

// Attach hidden Teleport button to DOM, add shortcut to button.
Drupal.Teleport.init = function() {
  var button = $("<a accesskey='t'>Teleport</a>");
  var teleport = $("<div id='teleport'></div>");
  var close = $("<a href='#'>Close</a>");
  
  teleport.html($(Drupal.settings.teleport.form));
  teleport.click(function() {$('#teleport #edit-title-wrapper INPUT').focus();});
  
  button.click(Drupal.Teleport.toggle);
  
  $("BODY").append(button);
  $('BODY').append(teleport);
  teleport.hide();
  button.hide();
  
  Drupal.behaviors.autocomplete('');

  close.click(function(){ teleport.hide(); return false; });
  $('FIELDSET', teleport).append(close);
};


Drupal.Teleport.toggle = function() {
  if ($('#teleport').css('display') == 'none') {
    Drupal.Teleport.appear();
  } else {
    Drupal.Teleport.vanish();
  }
};

Drupal.Teleport.appear = function() {
  $('#teleport').show("fast", Drupal.Teleport.focus);
};

Drupal.Teleport.vanish = function() {
  $('#teleport').hide("fast");
};

Drupal.Teleport.focus = function() {
  $('#teleport #edit-title')[0].focus();
  $('#teleport #edit-title')[0].select();
};

$(Drupal.Teleport.init);